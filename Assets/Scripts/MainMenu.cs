using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("GamePlay");
    }

    public void QuitGame()
    {
        Debug.Log("Quit button pressed!");
        Application.Quit();

        // For quitting in Unity Editor. Optional to remove in built application.
        // UnityEditor.EditorApplication.isPlaying = false;

    }
}
