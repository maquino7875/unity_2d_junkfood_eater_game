using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CharacterData : MonoBehaviour
{
    [SerializeField] Color32 characterColor = new Color32(1, 1, 1, 1);

    public void GetPlayerData()
    {
        //Debug.Log("GetPlayerData ran!");
        PlayerData playerData = new PlayerData();

        string json = JsonUtility.ToJson(playerData);

        SingletonGameManager.Instance.playerSelected = true;

    }

    private class PlayerData
    {
        public string characterName;
    }
}
