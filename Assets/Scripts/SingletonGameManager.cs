using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingletonGameManager : MonoBehaviour
{
    public static SingletonGameManager Instance { get; private set; }

    public bool playerSelected;
    public Color32 playerColor;
    public int lifeTime;
    public string timeText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (playerSelected == true)
        {
            // set player color here
            GameObject.Find("PlayButton").GetComponent<Button>().enabled = true;
        }
        // if button color pressed then  player color is that button color
    }
}
