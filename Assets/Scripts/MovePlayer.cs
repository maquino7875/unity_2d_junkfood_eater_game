using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float boostSpeed = 3f;
    [SerializeField] Rigidbody2D rb;
    [SerializeField] Animator animator;
    Vector2 movement;

    // Start is called before the first frame update
    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("Obstacle hit!");
        if (moveSpeed > 5f)
        {
            moveSpeed -= 1f;
        }

        animator.SetTrigger("Blocked");

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Food")
        {
            moveSpeed += boostSpeed;
            animator.SetTrigger("FoodPresent");
        }
    }

    void FixedUpdate()
    {
        // float verticalAmount = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
        // float horizontalAmount = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;
        // transform.Translate(verticalAmount, horizontalAmount, 0);

        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");

        rb.MovePosition(rb.position + movement * moveSpeed * Time.deltaTime);

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
    }
}
