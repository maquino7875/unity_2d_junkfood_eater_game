using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class colorSelector : MonoBehaviour
{
    public GameObject manager;
    // Start is called before the first frame update
    public void changeColor()
    {
        manager.GetComponent<SingletonGameManager>().playerColor = GetComponent<Image>().color;
    }
}
