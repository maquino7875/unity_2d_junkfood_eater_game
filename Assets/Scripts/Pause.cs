using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public SceneManagerScript SceneManager;
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Pause!");
        SceneManager.PauseGame();
    }
}
