using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MinMaxValue : MonoBehaviour
{
    // Set default min and max value to 1 and 100
    public float minValue = 1;
    public float maxValue = 100;
    private int value;
    public TMP_InputField inputField;

    public void OnValidate()
    {
        minValue = Mathf.Min(minValue, maxValue);
        maxValue = Mathf.Max(minValue, maxValue);
    }

    public void ReadTextInput()
    {
        value = int.Parse(inputField.text);
        
        // Set the input field to min or max value depending if greater than actual min or max values
        if (value > maxValue)
        {
            inputField.text = maxValue.ToString();
        }
        if (value < minValue)
        {
            inputField.text = minValue.ToString();
        }

    }
}
