using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class DiceRoller : MonoBehaviour
{

    public int numSides = 8;
    public int numRolls = 5;
    public int numHighRolls = 3;

    public TMP_InputField abilityField;
    public TMP_InputField displayField;

    private string textValue;

    public void DiceRoll()
    {
        List<int> list = new List<int>();
        string sum;

        // Add random numbers from 1 to number of sided die
        for (int i = 0; i < numRolls; i++)
        {
            list.Add(UnityEngine.Random.Range(1, numSides));           
        }

        // Add all numbers to a single string
        foreach (int number in list)
        {
            //Debug.Log(number);
            textValue += number + " ";
        }

        // List is in descended order and takes the number of high rolls, summing it, then converting it to string
        sum = list.OrderByDescending(x => x).Take(numHighRolls).Sum().ToString();

        abilityField.text = sum.ToString();

       //Debug.Log(textValue + "= " + sum);

        displayField.text += textValue + "= " + sum + "\n";

        textValue = "";

    }
}
