using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameDifficulty : MonoBehaviour
{
    [SerializeField] int lifeTime = 60;
    public GameObject manager;

    // Attach script to difficulty buttons. Will make it so life time is changeable in Unity editor.
    public void setDifficulty()
    {
        manager.GetComponent<SingletonGameManager>().lifeTime = lifeTime;
        Debug.Log("Time is set to:" + lifeTime);
    }
}
