using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consume : MonoBehaviour
{
    [SerializeField] float packageDelay = 1f;
    [SerializeField] AudioSource playSound;
    [SerializeField] int totalFood;
    [SerializeField] bool foodGone;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Food")
        {
            playSound.Play();
            Destroy(other.gameObject, packageDelay);
            totalFood -= 1;
            Debug.Log(totalFood);
            if (totalFood == 0) {
                foodGone = true;
            }
        }
    }
}
