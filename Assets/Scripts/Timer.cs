using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour
{
    bool timerActive;
    float currentTime;
    public int startMinutes;
    public Text currentTimeText, currentFoodText;
    public GameObject manager;

    [SerializeField] float packageDelay = 1f;
    [SerializeField] AudioSource playSound;
    [SerializeField] int totalFood;
    [SerializeField] bool foodGone;

    void Start()
    {
        currentFoodText.text = totalFood.ToString();
        currentTime = startMinutes * SingletonGameManager.Instance.lifeTime;
        timerActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerActive)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                timerActive = false;
                currentTime = 0;

            }
            TimeSpan time = TimeSpan.FromSeconds(currentTime);
            currentTimeText.text = time.Minutes.ToString() + ":" + time.Seconds.ToString() + ":" + time.Milliseconds.ToString();

            // Loads the won game scene when all food gone
            if (foodGone)
            {
                manager.GetComponent<SceneManagerScript>().WonGame();
            }
        }
        if (!timerActive)
        {
            if (!foodGone)
            {
                manager.GetComponent<SceneManagerScript>().PauseGame();
                // For when transfering remaining time to last run time in main menu scene
                SingletonGameManager.Instance.timeText = currentTimeText.text;
                currentTimeText.text = "GAME OVER!";
            }
        }
        // TimeSpan time = TimeSpan.FromSeconds(currentTime);
        // currentTimeText.text = time.Minutes.ToString() + ":" + time.Seconds.ToString() + ":" + time.Milliseconds.ToString();

        // currentTime -= Time.deltaTime;
        // if (currentTime <= 0 ) {
        //     timerActive = false;
        //     currentTime = 0;
        // }
        // TimeSpan time = TimeSpan.FromSeconds(currentTime);
        // currentTimeText.text = time.Minutes.ToString() + ":" + time.Seconds.ToString() + ":" +  time.Milliseconds.ToString();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Food")
        {
            playSound.Play();
            Destroy(other.gameObject, packageDelay);
            totalFood -= 1;
            Debug.Log(totalFood);
            currentFoodText.text = totalFood.ToString();
            if (totalFood <= 0)
            {
                foodGone = true;
            }
        }
    }

    public void StartTimer()
    {
        timerActive = true;
    }

    public void StopTimer()
    {
        timerActive = false;
    }
}
