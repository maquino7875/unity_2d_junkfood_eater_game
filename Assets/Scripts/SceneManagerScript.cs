using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("GamePlay");
    }

    public void LostGame()
    {
        Time.timeScale = 0;
    }

    public void WonGame()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Debug.Log("Quit button pressed!");
        Application.Quit();

        // For quitting in Unity Editor. Optional to remove in built application.
        // UnityEditor.EditorApplication.isPlaying = false;

    }
}
